var  express = require('express');
var rout = express.Router();
var signupcont = require('../controllers/signup-controller');
var mong = require('mongoose');

mong.connect("mongodb://localhost:27017/Twitteratis",function(err,db){
    if(!err){
        console.log("connected");
    }
});


rout.get('/',function(req, res){
    res.sendFile('signup.html', {root:'../frontend'});
});


rout.post('/signup',signupcont.signup);

module.exports = rout;